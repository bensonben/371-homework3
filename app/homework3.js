var vm = this;
var numClicks = 0;
var currentSelection, nextSelection = null;

var images = ["images/mario01.jpg",
    "images/mario02.jpg",
    "images/mario03.jpg",
    "images/mario04.jpg",
    "images/mario05.jpg",
    "images/mario06.jpg",
    "images/mario07.jpg",
    "images/mario08.jpg",
    "images/mario09.jpg",
    "images/mario10.jpg",
    "images/mario11.jpg",
    "images/mario12.jpg"
];

function randomizeImages() {
    for (var i = vm.images.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        swapPositions(i, j);
    }
}

function givePlayerDirections(){
	alert("Hello! unscramble the images");
}

function swapPositions(oldPosition, newPosition) {
    var temp = images[oldPosition];
    vm.images[oldPosition] = vm.images[newPosition];
    vm.images[newPosition] = temp;
}

function onLoad() {
    randomizeImages();
    givePlayerDirections();
    var htmlElementImages = document.querySelectorAll('#table tbody tr td img');
    for (var iterator = 0; iterator < htmlElementImages.length; iterator++) {
        htmlElementImages[iterator].src = images[iterator];
    }
}

function hasPlayerWon(){
	var result = true;
	for (var i = images.length - 1; i >= 0; i--) {
	    if(images[i].search((i+1).toString()) === -1){
	    	result = false;
	    }
	}
	return result;
}

function userSelected(currentPosition) {
    vm.numClicks += 1;
    if(undefined === vm.currentSelection || null === vm.currentSelection){
    	vm.currentSelection = currentPosition.toString();
    }
    if (vm.numClicks === 2 && currentSelection != null) {
        vm.nextSelection = currentPosition.toString();
        swapPositions(Number(vm.currentSelection), Number(vm.nextSelection));
        vm.numClicks = 0;
        document.getElementById(vm.currentSelection).src = vm.images[Number(vm.currentSelection)];
        document.getElementById(vm.nextSelection).src = vm.images[Number(vm.nextSelection)];
        vm.nextSelection = null;
        vm.currentSelection = null;
        if(hasPlayerWon()){
        	setTimeout(alert("congrats! you've unscrambled the image! The game will now reset!"), 2000);
        	randomizeImages();
        }
    } else if (vm.numClicks > 2) {
        vm.numClicks = 0;
        vm.currentSelection = null;
        vm.nextSelection = null;
    }
}
